var Q = require("utils/q.js");
var md5 = require("utils/md5.js");

App({
  /**
   * 全局
   */
  globalData: {
    // access_token 有效期为两个小时，考虑到网路的因素，可以将有效期设置成小于两个小时，此处为秒
    accessTokenLifeTime: 7000,
    // 请求
    hostAPI: 'https://homemapi.bmlhw.com',
    // 用户授权地址
    authUrl: '/pages/login/login',
    // 用户登录后台接口地址
    loginUrl: 'https://homemapi.bmlhw.com/v1s/provider/login',
    // 更新用户信息后台接口地址
    updateUrl: 'https://homemapi.bmlhw.com/v1s/provider/update',
    // TabBar，用于跳转的判断
    tabBar: [
      "/pages/index/index",
      "/pages/train/train",
      "/pages/user/order/order",
      "/pages/user/user"
    ],
    MAP_KEY: 'CANBZ-RUBLK-EUTJ5-AG5T3-YJJEZ-EAF4F',
    loadText: '数据加载中'
  },

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {

  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {

  },

  /**
   * 获取 access_token （access_token 服务端的有效期是两个小时）
   *
   * @param url 所在页面的 url
   * @param auth 针对用户主动授权用的，默认为 true
   */
  getAccessToken: function (url, auth) {
    var app = this;

    // 处理异步的问题
    var def = Q.defer();

    var lifeTime = app.globalData.accessTokenLifeTime * 1000;

    var authPageWithUrlOption = app.globalData.authUrl + '?url=';

    // 查看是否授权
    app.isAuthUserInfo(url, auth).then(function (url) {
      wx.getSetting({
        success: function (res) {
          if (res.authSetting['scope.userInfo']) { // 已经授权
            // 校验用户当前 session_key 是否有效
            wx.checkSession({
              success: function () {
                // 未过期
                try {
                  // 获取 access_token
                  var accessTokenObj = wx.getStorageSync('access_token');
                  if (accessTokenObj) {
                    var startTime = accessTokenObj.start_time;
                    if ((new Date()).getTime() - startTime < lifeTime) {
                      def.resolve(accessTokenObj.access_token);
                      app.jump(url);
                    } else {
                      // 清除过期的 access_token
                      wx.removeStorageSync('access_token');
                      // 需要重新登录
                      def = app.handleLogin(def, url);
                    }
                  } else {
                    // 缓存中不存在 access_token，需要重新登录
                    def = app.handleLogin(def, url);
                  }
                } catch (e) {
                  throw new Error(e.message);
                }
              },
              fail: function () {
                // 已经过期，需要重新登录
                def = app.handleLogin(def, url);
              }
            });
          } else { // 未授权
            // 跳转用户授权页面
            wx.navigateTo({
              url: authPageWithUrlOption + url
            });
          }
        }
      });
    });

    return def.promise;
  },

  /**
   * 是否授权过
   */
  isAuthUserInfo: function (url, auth) {
    var def = Q.defer();
    var bool = (auth === undefined ? true : auth);
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo'] && bool) {
          // 只要授权过，就将 url 置空字符串
          url = '';
          def.resolve(url);
        } else {
          def.resolve(url);
        }
      }
    });
    return def.promise;
  },

  /**
   * 登录，服务端返回 access_token 是无效的时候，需要用户重新登录
   * 重新登录可不传参
   */
  handleLogin: function (def, url) {
    var app = this;
    var loginUrl = app.globalData.loginUrl;

    def = (def === undefined ? Q.defer() : def);
    url = (url === undefined ? '' : url);

    // 登录
    wx.login({
      success: function (res) {
        if (res.code) {
          wx.request({
            url: loginUrl,
            data: {
              code: res.code
            },
            success: function (res) {
              var data = res.data;
              console.log(res)
              if (data.status === 1) {
                // 将 access_token 存入缓存
                try {
                  var accessTokenObj = {
                    access_token: data.data.access_token,
                    start_time: (new Date()).getTime()
                  };
                  wx.setStorageSync('access_token', accessTokenObj);
                  def.resolve(data.data.access_token);
                } catch (e) {
                  throw new Error(e.message);
                }

                app.jump(url);

                // 自动触发一下，更新用户信息
                app.handleUpdate(data.data.access_token);
              } else {
                wx.showToast({
                  title: data.msg
                });
              }
            }
          })
        } else {
          throw new Error(res.errMsg);
        }
      }
    });

    return def;
  },

  /**
   * 更新用户信息，用户成功登录后自动触发
   */
  handleUpdate: function (accessToken) {
    var app = this;
    var updateUrlPart = app.globalData.updateUrl + '?access_token=';

    wx.getUserInfo({
      success: function (res) {
        wx.request({
          url: updateUrlPart + accessToken,
          method: 'POST',
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          data: {
            encryptedData: res.encryptedData,
            iv: res.iv
          },
          success: function (res) {
            // 此处可以不处理
          }
        });
      }
    });
  },

  /**
   * 授权之后跳转（要判断跳转的是否为 TarBar）
   */
  jump: function (url) {
    if (url !== '') {
      if (this.isInArray(this.globalData.tabBar, url)) {
        wx.switchTab({
          url: url
        })
      } else {
        wx.navigateBack();
      }
    }
  },

  /**
   * 判断用户是否登录
   */
  isLogin: function () {
    // 处理异步的问题
    var def = Q.defer();
    var app = this;
    // 校验用户当前 session_key 是否有效
    wx.checkSession({
      success: function () {
        // 未过期
        try {
          // 获取 access_token
          var accessTokenObj = wx.getStorageSync('access_token');
          if (accessTokenObj) {
            var startTime = accessTokenObj.start_time;
            var lifeTime = app.globalData.accessTokenLifeTime * 1000;
            if ((new Date()).getTime() - startTime < lifeTime) {
              def.resolve(accessTokenObj.access_token);
            } else {
              def.resolve(false);
            }
          } else {
            def.resolve(false);
          }
        } catch (e) {
          throw new Error(e.message);
        }
      },
      fail: function () {
        def.resolve(false);
      }
    });

    return def.promise;
  },

  /**
   * 格式化当前页面的 Url
   */
  getPageUrl: function (p) {
    var route = p.pop().__route__;
    return '/' + route;
  },

  /**
   * 判断是否在数组中存在
   */
  isInArray: function (arr, value) {
    for (var i = 0; i < arr.length; i++) {
      if (value === arr[i]) {
        return true;
      }
    }
    return false;
  },
})
