function formatTime(timestamp, format) {　　
  const formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];　　
  let returnArr = [];　　
  let date = new Date(timestamp); //13位的时间戳,    如果不是13位的,  就要乘1000,就像这样 let date = new Date(timestamp*1000)
  let year = date.getFullYear()　　 
  let month = date.getMonth() + 1　　
  let day = date.getDate()　　 
  let hour = date.getHours()　　 
  let minute = date.getMinutes()　　 
  let second = date.getSeconds()　　 
  returnArr.push(year, month, day, hour, minute, second);　　
  returnArr = returnArr.map(formatNumber);　　
  for (var i in returnArr) {　　
    format = format.replace(formateArr[i], returnArr[i]);
  }　　
  return format;
}

const formatNumber = n => {　　
  n = n.toString()　　 
  return n[1] ? n : '0' + n
}


const formatTimeThr = (number, format) => {

  var formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];
  var returnArr = [];

  var date = new Date(number * 1000);
  returnArr.push(date.getFullYear());
  returnArr.push(formatNumber(date.getMonth() + 1));
  returnArr.push(formatNumber(date.getDate()));

  returnArr.push(formatNumber(date.getHours()));
  returnArr.push(formatNumber(date.getMinutes()));
  returnArr.push(formatNumber(date.getSeconds()));

  for (var i in returnArr) {
    format = format.replace(formateArr[i], returnArr[i]);
  }
  return format;
}

module.exports = {　　
  formatTime: formatTime
}