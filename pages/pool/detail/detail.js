// pages/pool/detail/detail.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNo: '',
    orderInfo: '',
    latitude: 0,
    longitude: 0,
    markers: [],
    hasMarkers: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderNo: options.trade_no
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getOrderInfo()
  },

  /**
   * 
   */
  onReady: function() {
    
  },

  /**
   * orders
   */
  getOrderInfo: function() {
    let that = this,
        markers = [],
        temp = {}
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/detail?access_token=' + accessToken,
        data: {
          out_trade_no: that.data.orderNo
        },
        success: res => {
          if(res.data.status === 1) {
            wx.getLocation({
              success: function(res) {
                wx.request({
                  url: app.globalData.hostAPI + '/v1/test/get-distance-by-lat-lng?access_token=' + accessToken,
                  data: {
                    out_trade_no: that.data.orderNo,
                    provider_lat: res.latitude,
                    provider_lng: res.longitude
                  },
                  success: res => {
                    temp = {
                      id: 1,
                      latitude: res.data.data.customer_lat,
                      longitude: res.data.data.customer_lng,
                      width:40,
                      height:40
                    }
                    markers.push(temp)
                    that.setData({
                      latitude: res.data.data.customer_lat,
                      longitude: res.data.data.customer_lng,
                      markers: markers,
                      hasMarkers: true,
                    })
                  }
                })
              },
            })
            that.setData({
              orderInfo: res.data.data
            })
          }
          wx.hideLoading()
        }
      })
    })
  },

  /**
   * 
   */
  receipt: function(e) {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/receipt?access_token=' + accessToken,
        data: {
          out_trade_no: that.data.orderNo
        },
        success: res => {
          if(res.data.status === 1) {
            wx.request({
              url: app.globalData.hostAPI + '/v1s/order/send-message?access_token=' + accessToken,
              data: {
                out_trade_no: that.data.orderNo
              },
              success: res1 => {
                console.log(res1)
              }
            })
            wx.showToast({
              title: '接单成功',
              duration: 2000,
              success:res => {
                wx.switchTab({
                  url: '/pages/order/order',
                })
              }
            })
          } else {
            wx.showToast({
              title: res.data.msg,
              duration:2000
            })
          }
        }
      })
    })
    // wx.showModal({
    //   title: '提示',
    //   content: '是否接单',
    //   success: res => {
    //     if(res.confirm) {

    //       wx.showLoading({
    //         mask: true,
    //         title: app.globalData.loadText,
    //       })

    //       app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
    //         wx.request({
    //           url: app.globalData.hostAPI + '/v1s/order/receipt?access_token=' + accessToken,
    //           data: {
    //             out_trade_no: that.data.orderNo
    //           },
    //           success: res => {
    //             if (res.data.status === 1) {
    //               wx.request({
    //                 url: app.globalData.hostAPI + '/v1s/order/send-message?access_token=' + accessToken,
    //                 data: {
    //                   out_trade_no: that.data.orderNo
    //                 },
    //                 success: res => {
    //                   if (res.data.status === 1) {
    //                     wx.hideLoading()
    //                     wx.showToast({
    //                       icon: 'none',
    //                       title: '接单成功',
    //                     })
    //                     that.getOrderList()
    //                   }
    //                 }
    //               })
    //               wx.hideLoading()
    //               wx.showToast({
    //                 icon: 'none',
    //                 title: '接单成功',
    //               })
    //               wx.switchTab({
    //                 url: '/pages/order/order?status=1',
    //               })
    //             } else {
    //               wx.showToast({
    //                 icon: 'none',
    //                 title: '接单出错',
    //               })
    //               that.getOrderInfo()
    //             }
    //           }
    //         })
    //       })
    //     }
    //   }
    // })
  }
})