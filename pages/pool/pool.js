// pages/pool/pool.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList: [],
    page: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.init(this.data.page)
  },
  
  /**
   * init
   */
  init: function(page) {
    let that = this,
        orderList = that.data.orderList,
        k

    wx.showLoading({
      title: app.globalData.loadText,
      mask: true,
    })

    wx.getLocation({
      success: function(res) {
        console.log(res)
        app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
          wx.request({
            url: app.globalData.hostAPI + '/v1s/order/receipt-pool-new?access_token=' + accessToken,
            data: {
              page: that.data.page,
              provider_lat: res.latitude,
              provider_lng: res.longitude
            },
            success: res => {
              if (res.data.status === 1) {
                if(page == 1) {
                  that.setData({
                    orderList: res.data.data
                  }, () => {
                    wx.hideLoading()
                  })
                } else {
                  if(res.data.data.length != 0) {
                    
                    for (k in res.data.data) {
                      orderList.push(res.data.data[k])
                    }

                    that.setData({
                      orderList: orderList
                    }, () => {
                      wx.hideLoading()
                    })
                  } else {
                    wx.showToast({
                      title: '没有更多',
                    })
                  }
                }
              }
            }
          })
        })
      },
    })
  },

  getDetail: function(e) {
    let order_no = e.currentTarget.dataset.trade
    wx.navigateTo({
      url: './detail/detail?trade_no=' + order_no,
    })
  },

  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    this.init(1)
    wx.hideNavigationBarLoading()
    wx.stopPullDownRefresh()
  },

  onReachBottom: function() {
    let that = this,
        page = that.data.page
    page = page + 1
    that.setData({
      page: page
    })
    that.init(that.data.page)
  }
})