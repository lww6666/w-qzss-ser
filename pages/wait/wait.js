// pages/wait/wait.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    waitStatus: '',
    status: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      status: options.status
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    if (that.data.status == 0) {
      that.setData({
        waitStatus: '审核中',
      })
    }

    if (that.data.status == 1) {
      that.setData({
        waitStatus: '审核通过',
      })
    }

    if (that.data.status == 2) {
      that.setData({
        waitStatus: '审核不通过',
      })
    }
  },

  
})