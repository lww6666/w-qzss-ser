// pages/user/changepass/changepass.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: {
      mobile: '',
      captcha: '',
      password: '',
      confirm_password: ''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getMobile()
  },

  /**
   * getMobile
   */
  getMobile: function() {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-pass?access_token=' + accessToken,
        success: res => {
          if(res.data.status === 1) {
            that.setData({
              'info.mobile': res.data.data.mobile
            })
            wx.hideLoading()
          }

          if (res.data.status == -10) {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '审核未通过，请重新审核',
              success: res => {
                if (res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * telValue
   */
  telValue: function({ detail }) {
    this.setData({
      'info.mobile': detail.value
    })
  },

  /**
   * smsValue
   */
  smsValue: function({ detail }) {
    this.setData({
      'info.captcha': detail.value
    })
  },

  /**
   * passwordValue
   */
  passwordValue: function ({ detail }) {
    this.setData({
      'info.password': detail.value
    })
  },

  /**
   * resPasswordValue
   */
  rePasswordValue: function ({ detail }) {
    this.setData({
      'info.confirm_password': detail.value
    })
  },

  /**
   * send sms
   */
  sendSms: function() {
    let that = this
    if (that.data.info.mobile == '') {
      wx.showToast({
        icon: 'none',
        title: app.globalData.loadText,
      })

      return false
    }

    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/send-sms-change-pass?access_token=' + accessToken,
        data: {
          mobile: that.data.info.mobile
        },
        success: res => {
          if(res.data.status === 1) {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '验证码已发送',
            })
          } else {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: res.data.msg,
            })
          }
        }
      })
    })
  },

  /**
   * modifyPwd
   */
  modifyPwd: function() { 
    let that = this,
        info = that.data.info
    
    if (info.mobile === '') {
      wx.showToast({
        icon: 'none',
        title: '请输入手机号',
      })

      return false
    }

    if (!(/^1[34578]\d{9}$/).test(info.mobile)) {
      wx.showToast({
        icon: 'none',
        title: '手机号码不正确',
      })

      return false
    }

    if (info.password === '') {
      wx.showToast({
        icon: 'none',
        title: '请输入密码',
      })

      return false
    }

    if (info.confirm_password === '') {
      wx.showToast({
        icon: 'none',
        title: '请再次输入密码',
      })

      return false
    }

    if (info.confirm_password !== info.password) {
      wx.showToast({
        icon: 'none',
        title: '两次输入密码不一致',
      })
      return false
    }

    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-pass?access_token=' + accessToken,
        method: 'POST',
        data: {
          info: JSON.stringify(info)
        },
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        success: res => {
          if(res.data.status === 1) {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '修改信息成功',
              success: res => {
                if(res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          } else {
            wx.hideLoading()
            wx.showToast({
              icon: 'none',
              title: res.data.msg,
            })
          }
        }
      })
    })
  }
})