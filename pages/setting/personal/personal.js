// pages/user/setting/setting.js
var app = getApp()
var time = require('../../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    dateTimeShow: false,
    genderShow: false,
    minDate: new Date(1900,1,1).getTime(),
    maxDate: new Date().getTime(),
    currentDate: new Date(1990,1,1).getTime(),
    gender: [
      {value: 0, name: '男'},
      {value: 1, name: '女' },
    ],
    info: {
      nickname: '',
      gender: '',
      birth_year: '',
      birth_month: '',
      birth_day: ''
    },
    username: '',
    datetime: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getProfile()
  },

  /**
   * 获取信息
   */
  getProfile: function() {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-profile?access_token=' + accessToken,
        success: res => {
          if(res.data.status === 1) {
            if(res.data.status === 1) {
              that.setData({
                info: res.data.data
              })
            }
          }
        }
      })
    })
  },

  /**
   * 
   */
  changeUserName: function() {
    let that = this
    that.setData({
      show: true
    })
  },

  onClose: function() {
    let that = this
    that.setData({
      show: false,
      genderShow: false
    })
  },

  /**
   * 
   */
  selectSix: function() {
    this.setData({
      genderShow: true,
    })
  },

  /**
   * selectDateTime
   */
  selectTime: function() {
    let that = this
    that.setData({
      dateTimeShow: true
    })
  },

  datetimeClose: function() {
    let that = this
    that.setData({
      dateTimeShow:false
    })
  },

  /**
   * 选择时间
   */
  onChange: function(e) {
    let that = this,
        datetime = time.formatTime(e.detail, 'Y-M-D'),
        strArr = datetime.split('-')
    that.setData({
      'info.birth_year': strArr[0],
      'info.birth_month': strArr[1],
      'info.birth_day': strArr[2],
      dateTimeShow: false
    })
  },

  /**
   * 
   */
  nameValue: function(e) {
    this.setData({
      username: e.detail.value
    })
  },

  /**
   * 获取名字
   */
  getName: function() {
    let that = this
    if(that.data.username == '') {
      wx.showToast({
        icon: 'none',
        title: '请输入姓名',
      })

      return false
    }

    this.setData({
      'info.nickname': that.data.username,
      show: false
    })
  },

  /**
   * 
   */
  saveProfile: function() {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-profile?access_token=' + accessToken,
        method: 'POST',
        data: {
          info: that.data.info
        },
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        success: res => {
          if(res.data.status === 1) {
            console.log(res)
          }
        }
      })
    })
  },

  /**
   * radioChange
   */
  radioChange: function(e) {
    let that = this
    that.setData({
      'info.gender': e.detail.value,
      genderShow: false
    })
  },
})