// pages/recharge/list/list.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isInteger: true,
    financeList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initApp()
  },

  /**
   * 
   */
  initApp: function() {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/flow-balance?access_token=' + accessToken,
        success: res => {
          if (res.data.status === 1) {
            that.setData({
              financeList: res.data.data
            })
            wx.hideLoading()
          } else {
            wx.hideLoading()
            wx.showToast({
              icon: 'none',
              title: res.data.msg,
              success: res => {
                wx.switchTab({
                  url: '../user/user',
                })
              }
            })
          }
        }
      })
    })
  },

  /**
   * 
   */
  getMore: function(e) {
    let trade_no = e.currentTarget.id
    wx.navigateTo({
      url: '../detail/detail?trade_no=' + trade_no,
    })
  }
})