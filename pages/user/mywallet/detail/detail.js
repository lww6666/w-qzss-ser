// pages/recharge/detail/detail.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 加载详情
   */
  getDetail: function(tread_no) {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/paid-page/recharge?access_token=' + accessToken,
        data: {
          out_trade_no: tread_no
        },
        success: res => {
          if(res.data.status === 1) {
            that.setData({
              detail: res.data.data
            })
          }
          wx.hideLoading()
        }
      })
    })
  }
})