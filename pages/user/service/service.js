// pages/user/service/service.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    catsItem: {},
    selected_ser_cat_ids:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getCatsList()
  },

  /*
   * 初始化
   */
  getCatsList: function() {
    let that = this,
        catsItem = [],
        k, j, data
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-ser?access_token=' + accessToken,
        success: res => {
          data = res.data.data
          if(res.data.status === 1) {
            for (k in data.ser_cats) {
              for (j in data.ser_cats[k].child) {
                catsItem.push(data.ser_cats[k].child[j])
              }
            }
            that.setData({
              catsItem: catsItem,
              selected_ser_cat_ids: res.data.data.selected_ser_cat_ids
            })
            wx.hideLoading()
          }

          if (res.data.status == -10) {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '审核未通过，请重新审核',
              success: res => {
                if (res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * 选择技能
   */
  changeSkill: function (e) {
    let ids = e.detail.value
    this.setData({
      selected_ser_cat_ids: ids
    })
  },
  /**
   * 提交
   */
  skillSubmit: function() {
    let that = this,
        ids = that.data.selected_ser_cat_ids
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-ser?access_token=' + accessToken,
        method: 'POST',
        data: {
          selected_ser_cat_ids: ids.join()
        },
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        success: res => {
          if(res.data.status === 1) {
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '更新信息成功',
            })
          } else {
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '稍后再试',
            })
          }
          wx.hideLoading()
        }
      })
    })
  }
})