// pages/user/address/add/add.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    provinceList: [],
    cityList: [],
    areaList: [],
    address: {
      addId: 0,
      userName: '',
      provId: 0,
      selProvName: '请选择省份',
      cityId: 0,
      selCityName: '请选择市',
      areaId: 0,
      selAreaName: '请选择区',
      telphone: '',
      address: '',
      smsCode: '',
      isDefault: 0,
    },
    btnText: '添加地址'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    that.getProvince()
    if (options.hasOwnProperty('add_id')) {
      that.setData({
        btnText: '修改地址',
      })
      that.getDefault(options.add_id)
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //this.initApp()
  },

  /**
   * 获取省份
   */
  getProvince: function() {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI +  '/v1/region/province?access_token=' + accessToken,
        success: res => {
          that.setData({
            provinceList: res.data.data.province,
          })
          wx.hideLoading()
        }
      })
    })
  },

  /**
   * 选择省份
   */
  changeProvince: function(e) {
    let that = this,
        i = e.detail.value,
        pid,name
    name = that.data.provinceList[i].fullname
    pid = that.data.provinceList[i].id
    that.setData({
      'address.provId': pid,
      'address.selProvName': name
    })
    that.getCity(pid)
  },

  /**
   * 获取市
   */
  getCity: function(parentId) {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/region/city?access_token=' + accessToken + '&province_id=' + parentId,
        success: res => {
          that.setData({
            cityList: res.data.data.city
          })
          wx.hideLoading()
        }
      })
    })
  },

  /**
   * 城市
   */
  changeCity: function (e) {
    let that = this,
        i = e.detail.value,
        cid,name
    cid = that.data.cityList[i].id
    name = that.data.cityList[i].fullname
    that.setData({
      'address.cityId': cid,
      'address.selCityName': name
    })
    that.getArea(cid)
  },

  /**
   * 获取区
   */
  getArea: function(cityId) {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/region/district?access_token=' + accessToken + '&city_id=' + cityId,
        success: res => {
          that.setData({
            areaList: res.data.data.district
          })
          wx.hideLoading()
        }
      })
    })
  },

  /**
   * 选择区
   */
  changeArea: function (e) {
    let that = this,
        i = e.detail.value,
        aid,name
    aid = that.data.areaList[i].id
    name = that.data.areaList[i].fullname
    that.setData({
      'address.areaId': aid,
      'address.selAreaName': name
    })
  },

  /**
   * 是否默认
   */
  radioChange: function(e) {
    this.setData({
      'address.isDefault': e.detail.value
    })
  },

  /**
   * 获取userName的值
   */
  nameInputValue: function ({ detail }) {
    this.setData({
      'address.userName': detail
    })
  },

  /**
   * 获取telPhone的值
   */
  telInputValue: function({detail}) {
    this.setData({
      'address.telphone': detail
    })
  },

  /**
   * 获取address的值
   */
  addressInputValue: function({detail}) {
    this.setData({
      'address.address': detail
    })
  },

  /**
   * 验证并提交地址
   */
  checkSubmitAddress: function() {
    let that = this,
        address = that.data.address,
        telReg = /(^1[3|4|5|7|8|9]\d{9}$)|(^09\d{8}$)/

    if (address.userName === '') {
      wx.showToast({
        icon: 'none',
        title: '请输入姓名',
      })
      return false
    }

    if (address.provId === 0) {
      wx.showToast({
        icon: 'none',
        title: '请选择省份',
      })
      return false
    }

    if (address.cityId === 0) {
      wx.showToast({
        icon: 'none',
        title: '请选择市级',
      })
      return false
    }

    if (address.areaId === 0) {
      wx.showToast({
        icon: 'none',
        title: '请选择辖区',
      })
      return false
    }

    if (address.address === '') {
      wx.showToast({
        icon: 'none',
        title: '请输入详细地址',
      })
      return false
    }

    if (address.telphone === '') {
      wx.showToast({
        icon: 'none',
        title: '请输入联系电话',
      })
      return false
    }

    if (!telReg.test(address.telphone)) {
      wx.showToast({
        icon: 'none',
        title: '号码不正确',
      })
      return false
    }
    
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/addr/add?access_token=' + accessToken,
        method: 'POST',
        data:{
          name: address.userName,
          province_id: address.provId,
          city_id: address.cityId,
          district_id: address.areaId,
          address: address.address,
          phone: address.telphone,
          is_default: address.isDefault
        },
        header:{
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: res => {
          if(res.data.status === 1) {
            wx.navigateBack()
          }
        }
      })
    })
  },

  /**
   * 
   */
  getDefault: function(id) {
    let that = this,
        address = that.data.address,
        newAddress = [],
        data
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/addr/get-by-id?access_token=' + accessToken,
        data: {
          addr_id: id
        },
        success: res => {
          data = res.data.data
          if(res.data.status === 1) {
            that.setData({
              'address.addId': data.id,
              'address.userName': data.name,
              'address.provId': data.province_id,
              'address.cityId': data.city_id,
              'address.areaId': data.district_id,
              'address.address': data.address,
              'address.telphone': data.phone,
              'address.isDefault': data.is_default
            })
          }
          wx.hideLoading()
        }
      })
    })
  },
})