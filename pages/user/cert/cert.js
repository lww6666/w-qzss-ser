// pages/user/cert/cert.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: {
      id_card_image1: '',
      id_card_image2: '',
      health_certificate: '',
      qual_certificate: '',
      home_train_certificate: ''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getMobile()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(111)
  },

  /**
   * 获取手机号码
   */
  getMobile: function() {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-qual?access_token=' + accessToken,
        success: res => {
          if(res.data.status === 1) {
            that.setData({
              info: res.data.data.info
            })
          }

          if (res.data.status == -10) {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '审核未通过，请重新审核',
              success: res => {
                if (res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * 上传方法
   * 
   * 1 身份证正面 2 身份证反面 3 健康证 4 从业资格证 5 家政培训
   * 
   */
  upLoad: function (e) {
    console.log(e)
    let that = this,
        bs = e.currentTarget.dataset.bs,
        data, tempFilePaths
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.chooseImage({
        success(res) {
          tempFilePaths = res.tempFilePaths
          wx.uploadFile({
            url: app.globalData.hostAPI + '/v1s/provider/upload?access_token=' + accessToken, 
            filePath: tempFilePaths[0],
            name: 'file',
            success(res) {
              data = JSON.parse(res.data)
              console.log(data)
              if (data.status === 1) {
                that.setData({
                  [bs]: data.data.url
                })
              } else {
                wx.showToast({
                  icon: 'none',
                  title: res.msg,
                })
              }
            }
          })
        }
      })
    })
  },

  /**
   * delCert
   */
  delCert: function (e) {
    let that = this,
        bs = e.currentTarget.dataset.bs
    console.log(bs)
    that.setData({
      [bs]: ""
    })
  },

  /**
   * submit
   */
  submitQaul: function() {
    let that = this,
        data = that.data
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/change-qual?access_token=' + accessToken,
        method: 'POST',
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          info: JSON.stringify(that.data.info)
        },
        success: res => {
          if(res.data.status === 1) {
            wx.hideLoading()
            wx.showModal({
              title: '提示',
              content: '信息修改成功',
              success: res => {
                if(res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          }
        }
      })
    })
  }
})