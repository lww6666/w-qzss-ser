// pages/user/myenroll/myenroll.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pushList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getList: function() {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/order-push-list?access_token=' + accessToken,
        success: res => {
          that.setData({
            pushList: res.data.data
          })
        }
      })
    })
  },

  goDetail: function (e) {
    wx.navigateTo({
      url: './detail?mid=' + e.currentTarget.dataset.mid,
    })
  }
})