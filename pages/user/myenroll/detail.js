// pages/enroll/detail/detail.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: {},
    disabled: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getDetail(options.mid)
    // let _this = this,
    //     random = Math.floor(Math.random() * 100)
    // if (random < 60) _this.getDetail(options.mid)
    // else _this.error()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  error: function () {
    wx.showLoading({
      title: '加载数据',
    })
    setTimeout(() => {
      wx.hideLoading()
      wx.showToast({
        title: '请求超时',
        duration: 3000
      })
    }, 5000)
  },

  getDetail: function (mid) {
    let _this = this,
      detail = {},
      images = [],
      new_images = []
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order-push/detail?access_token=' + accessToken,
        data: {
          id: mid
        },
        success: res => {
          if (res.data.status === 1) {
            images = JSON.parse(res.data.data.images)
            for (let k in images) {
              if (images[k] != "/images/upload.png") {
                new_images.push(images[k])
              }
            }
            detail = res.data.data
            detail.images = new_images
            _this.setData({
              detail: detail,
              disabled: false
            })
          }
        }
      })
    })
  },

  enrollClick: function () {
    let _this = this
    wx.showModal({
      title: '提示',
      content: '是否确认报名',
      success: res => {
        if (res.confirm) {
          app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
            wx.request({
              url: app.globalData.hostAPI + '/v1s/order-push/signing?access_token=' + accessToken,
              data: {
                id: _this.data.detail.id
              },
              success: data => {
                if (data.data.status === 1) {
                  wx.showModal({
                    title: '提示',
                    content: '报名成功',
                    showCancel: false,
                    success: res => {
                      if (res.confirm) {
                        wx.navigateTo({
                          url: '/pages/user/myenroll/myenroll',
                        })
                      }
                    }
                  })
                  if (data.data.data.send_message === 1) {
                    console.log("发信息")
                  }
                } else {
                  wx.showModal({
                    title: '提示',
                    content: data.data.msg,
                    showCancel: false
                  })
                }
              }
            })
          })
        }
      }
    })
  },

  previewImg: function (e) {
    let that = this;
    wx.previewImage({
      current: e.currentTarget.dataset.src,     //当前图片地址
      urls: that.data.detail.images,               //所有要预览的图片的地址集合 数组形式
    })
  }
})