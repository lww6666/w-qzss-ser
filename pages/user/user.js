// pages/user/user.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    provider: {
      
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getProvider()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 
   */
  getProvider: function() {
    let that = this,
        data
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/provider/index?access_token=' + accessToken,
        success: res => {
          data = res.data.data
          if(res.data.status === 1) {
            that.setData({
              provider: res.data.data
            })
            wx.hideLoading()
          }
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '登陆出错',
          })
        }
      })
    })
  },

  /**
   * 查看审核进度
   */
  status: function() {
    let that = this
    wx.navigateTo({
      url: '../wait/wait?status=' + that.data.provider.status,
    })
    // if(that.data.provider.status == 0) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '待审核',
    //   })
    // } else if (that.data.provider.status == 1) {
    //   wx.showModal({
    //     title: '提示',
    //     content: '您已经是保洁了',
    //   })
    // } else {
    //   wx.showModal({
    //     title: '提示',
    //     content: '重新提交审核',
    //   })
    // }
  }
})