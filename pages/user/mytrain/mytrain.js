// pages/user/mytrain/mytrain.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoList: [],
    isVideo: false,
    videoSrc: '',
    activeNames: ['1']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getVedioList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  playVideo: function(e) {
    const src = e.currentTarget.dataset.src
    this.setData({
      videoSrc: src,
      isVideo: true
    })
  },
  getVedioList: function () {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/video/bought-list?access_token=' + accessToken,
        success: res => {
          if (res.data.status === 1) {
            that.setData({
              videoList: res.data.data
            })
          }
        }
      })
    })
  },

  onChange: function(e) {
    this.setData({
      activeNames: e.detail,
    });
  },

  hideMask: function() {
    this.setData({
      isVideo: false
    })
  }
})