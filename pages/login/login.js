// pages/login/login.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      url: options.url
    })
  },

  /**
   * 
   */
  onGetUserInfo: function() {
    let that = this
    app.getAccessToken(that.data.url, false)
  },

  /**
   * 
   */
  cancel: function() {
    wx.switchTab({
      url: '../index/index',
    })
  }
})