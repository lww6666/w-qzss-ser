// pages/exam/success/fail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    anwserEnd: {},
    errorList: [],
    time: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const end = JSON.parse(options.end_data)
    this.setData({
      anwserEnd: end,
      errorList: end.error_questions,
      time: this.getNowFormatDate
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  agian: function(e) {
    wx.navigateTo({
      url: '/pages/train/train',
    })
  },

  getNowFormatDate: function() {
    var date = new Date(),
        seperator1 = '/',
        year = date.getFullYear(),
        month = date.getMonth() + 1,
        strDate = date.getDate()
    if(month >= 1 && month <= 9) {
      month = "0" + month
    }
    if(strDate >= 0 && strDate <= 9) {
      strDate = '0' + strDate
    }
    const currentDate = year + seperator1 + month + seperator1 + strDate
    return currentDate
  }
})