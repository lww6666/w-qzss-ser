// pages/exam/exam.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    examObj: {},
    answer: "",
    answerObj: {},
    answerEnd: {},
    id: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id: options.id
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getExam()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // onChange(field,e) {
  //   this.setData({
  //     [field]: e.detail.value
  //   })
  //   console.log('radio发生change事件，携带value值为：', e.detail.value)
  // },

  onChange: function(e) {
    console.log(e)
    this.setData({
      answer: e.detail,
      answerObj: e.detail
    })
  },

  getExam: function() {
    let _this = this
    wx.showLoading({
      title: '数据加载中',
      mask: true
    })
    this.setData({
      answer: '',
      answerObj: {}
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/question/received?access_token=' + accessToken,
        data: {
          category_id: _this.data.id
        },
        success: res => {
          if(res.data.status === 1) {
            if(res.data.data.final === 1) {
              _this.setData({
                answerEnd: res.data.data
              })
              if(res.data.data.point > 80) {
                wx.navigateTo({
                  url: './success/success?end_data=' + JSON.stringify(_this.data.answerEnd) + '&category_id=' + _this.data.id,
                })
              } else {
                wx.navigateTo({
                  url: './success/fail?end_data=' + JSON.stringify(_this.data.answerEnd),
                })
              }
            } else {
              _this.setData({
                examObj: res.data.data
              })
              wx.hideLoading()
            }
          } else {
            wx.showToast({
              title: res.data.msg,
              duration: 2000
            })
          }
        }
      })
    })
  },

  setObj: function(e) {
    let { obj } = e.currentTarget.dataset
    this.setData({
      answer: obj.options,
      answerObj: obj
    })
  },

  doExam: function() {
    let ansobj = this.data.answerObj,
        exmobj = this.data.examObj,
        _this = this

    if(this.data.answer === "") {
      wx.showToast({
        title: '请选择答案',
        duration: 2000
      })
      return false
    }

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/question/answer?access_token=' + accessToken,
        method: 'POST',
        data: {
          question_id: exmobj.question.id,
          question_name: exmobj.question.name,
          response_answer_id: ansobj.id,
          response_answer_option: ansobj.options,
          category_id: _this.data.id
        },
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        success: res => {
          if(res.data.status === 1) {
            console.log(res.data)
            _this.getExam()
          } else {
            wx.showToast({
              title: res.data.msg,
              duration: 2000
            })
          }
        }
      })
    })
  }
})