// pages/order/order.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderActive: 0,
    orderList: [],
    page: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this
    _this.getOrderList(_this.data.orderActive, _this.data.page)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 获取订单列表
   */
  getOrderList: function(order_type,page) {
    let that = this, 
        list = [],
        data,k

    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/list?access_token=' + accessToken,
        data: {
          status: order_type,
          page: page,
        },
        success: res => {
          data = res.data.data
          if(res.data.status === 1) {
            
            for (k in data) {
              if (data[k].status == 0 ) {
                data[k].address = '**********'
              }
            }
            
            that.setData({
              orderList: res.data.data
            })
            wx.hideLoading()
          }

          if (res.data.status == -10) {
            wx.hideLoading()
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '审核未通过，请重新审核',
              success: res => {
                if (res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * 
   */
  orderChange: function(e) {
    let oid = e.currentTarget.dataset.status
    this.setData({
      orderActive: oid
    })
    this.getOrderList(this.data.orderActive,this.data.page)
  },

  /**
  * 上拉刷新
  */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.getOrderList(this.data.orderActive, this.data.page)
    wx.hideNavigationBarLoading()
    wx.stopPullDownRefresh()
  },

  /**
   * 上拉加载更多
   */
  // onReachBottom: function () {
  //   let that = this,
  //     page = that.data.page
  //   page = page + 1
  //   that.setData({
  //     page: page
  //   })
  //   that.getOrderList(that.data.orderActive, that.data.page)
  // },

  getDetail: function (e) {
    let order_no = e.currentTarget.dataset.trade
    wx.navigateTo({
      url: '../pool/detail/detail?trade_no=' + order_no,
    })
  },

  receipt: function (e) {
    let that = this,
      order_no = e.currentTarget.dataset.orderno
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/receipt?access_token=' + accessToken,
        data: {
          out_trade_no: order_no
        },
        success: res => {
          if (res.data.status === 1) {
            wx.hideLoading()
            that.getOrderList()
            wx.request({
              url: app.globalData.hostAPI + '/v1s/order/send-message?access_token=' + accessToken,
              data: {
                out_trade_no: order_no
              },
              success:res => {
                if(res.data.status === 1) {
                  wx.showToast({
                    icon: 'none',
                    title: '接单成功',
                  })
                }
              }
            })
          } else {
            wx.showToast({
              icon: 'none',
              title: '接单出错',
            })
            that.getOrderList(that.data.orderActive, that.data.page)
          } 
        }
      })
    })
  },

  /**
   * 
   */
  cancel: function(e) {
    let that = this,
      order_no = e.currentTarget.dataset.orderno

    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/cancel?access_token=' + accessToken,
        data: {
          out_trade_no: order_no
        },
        success: res => {
          if (res.data.status === 1) {
            wx.hideLoading()
            wx.showToast({
              icon: 'none',
              title: '接单成功',
            })
            that.getOrderList()
          } else {
            wx.showToast({
              icon: 'none',
              title: '接单出错',
            })
            that.getOrderList(that.data.orderActive, that.data.page)
          }
        }
      })
    })
  },

  /**
   * 签到
   */
  working: function(e) {
    let that = this,
        order_no = e.currentTarget.dataset.orderno

    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/signin?access_token=' + accessToken,
        data: {
          out_trade_no: order_no
        },
        success: res => {
          if(res.data.status === 1) {
            wx.hideLoading()
            wx.showModal({
              title: '提示',
              content: '开始服务',
              showCancel: false,
              success: res => {
                if(res.confirm) {
                  that.getOrderList(that.data.orderActive, that.data.page)
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * 完成
   */
  finish: function(e) {
    let that = this,
      order_no = e.currentTarget.dataset.orderno

    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })

    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order/finished?access_token=' + accessToken,
        data: {
          out_trade_no: order_no
        },
        success: res => {
          if (res.data.status === 1) {
            wx.hideLoading()
            wx.showModal({
              title: '提示',
              content: '服务完成',
              showCancel: false,
              success: res => {
                if(res.confirm) {
                  that.getOrderList(that.data.orderActive, that.data.page)
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * 打电话
   */
  makeCall: function(e) {
    let phone = e.currentTarget.dataset.tel
    if(phone != null) {
      wx.makePhoneCall({
        phoneNumber: phone,
      })
    } else {
      wx.showToast({
        icon: 'none',
        title: '预留电话错误',
      })
    }
    
  }
})