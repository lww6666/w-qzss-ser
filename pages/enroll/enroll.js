// pages/enroll/enroll.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pushList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  getList: function() {
    let _this = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/order-push/pool?access_token=' + accessToken,
        success: res => {
          _this.setData({
            pushList: res.data.data
          })
        }
      })
    })
    
  },

  /**
   * 
   */
  goDetail: function(e) {
    wx.navigateTo({
      url: '/pages/enroll/detail/detail?mid=' + e.currentTarget.dataset.mid,
    })
  }
})