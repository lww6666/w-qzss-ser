// pages/index/index.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderStatus: 1,
    hiding: true,
    bannerList: [],
    courseList: [],
    orders: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initApp()
  },

  /**
   * 初始化
   */
  initApp: function () {
    let that = this
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/index/index?access_token=' + accessToken,
        success: res => {
          if(res.data.status === 1) {
            that.setData({
              bannerList: res.data.data.banners,
              courseList: res.data.data.courses,
              orders: res.data.data.orders
            })
          }
          if(res.data.status == -10) {
            wx.showModal({
              showCancel: false,
              title: '提示',
              content: '审核未通过，请重新审核',
              success: res => {
                if(res.confirm) {
                  wx.switchTab({
                    url: '/pages/user/user',
                  })
                }
              }
            })
          }
        }
      })
    })
  },

  /**
   * 
   */
  hidingClick: function() {
    let that = this
    wx.showModal({
      title: '提示',
      content: '确定要切换离线状态？',
      success: res => {
        if(res.confirm) {
          if(that.data.hiding) {
            wx.showLoading({
              mask: true,
              title: app.globalData.loadText,
            })
            app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
              wx.request({
                url: app.globalData.hostAPI + '/v1s/index/edit-status-e?access_token=' + accessToken,
                success: res => {
                  if(res.data.status === 1) {
                    that.setData({
                      hiding: false
                    })
                  }
                  wx.hideLoading()
                }
              })
            })
          } else {
            app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
              wx.request({
                url: app.globalData.hostAPI + '/v1s/index/edit-status-e?access_token=' + accessToken,
                success: res => {
                  if (res.data.status === 1) {
                    that.setData({
                      hiding: true
                    })
                  }
                  wx.hideLoading()
                }
              })
            })
            
          }
        }
      }
    })
  },

  grabOrder: function() {
    wx.navigateTo({
      url: '../pool/pool',
    })
  },

  enroll: function(e) {
    let eid = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../enroll/detail/detail?eid=' + eid,
    })
  },

  /**
   * 
   */
  getDetail: function (e) {
    let order_no = e.currentTarget.dataset.trade
    wx.navigateTo({
      url: '../order/order',
    })
  },

  onShareAppMessage: function() {
    return {
      title: '7只仓鼠保洁端'
    }
  }
})