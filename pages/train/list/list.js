// pages/train/traindetail/traindetail.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    vedioList: [],
    isExam: true,
    id: 0,
    cid: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id: options.id,
      cid: options.cid
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getVedioList(this.data.id)
  },

  /**
   * 
   */
  getVedioList: function (cid) {
    let that = this
    const videoList = []
    app.getAccessToken(app.getPageUrl(getCurrentPages())).then(function (accessToken) {
      wx.request({
        url: app.globalData.hostAPI + '/v1s/video/index?access_token=' + accessToken + '&course_id=' + cid,
        success: res => {
          const data = res.data.data
          if (res.data.status === 1) {
            for (const k in data) {
              if (data[k].is_bought !== 1) {
                videoList.push(data[k])
              }
            }
            that.setData({
              vedioList: videoList
            })
            if(videoList.length === 0) {
              that.setData({
                isExam: false
              })
            }
          }
        }
      })
    })
  },

  tabs: function (e) {
    wx.redirectTo({
      url: '/pages/train/detail/detail?id=' + e.currentTarget.dataset.id,
    })
  },

  toExam: function() {
    wx.navigateTo({
      url: '/pages/exam/exam?id=' + this.data.cid,
    })
  }
})