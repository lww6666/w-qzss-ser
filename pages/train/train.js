// pages/train/train.js
var app = getApp()
var WxParse = require('../../wxParse/wxParse.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentIndex: 0,
    aboutTitle: '',
    aboutContent: '',
    aboutImg: '',
    courseList: [],
    bmList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    wx.showLoading({
      mask: true,
      title: app.globalData.loadText,
    })
    that.initApp(that.data.currentIndex)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  tabbarChange: function (e) {
    let that = this
    let index = e.target.dataset.index
    if (that.data.currentIndex === index) {
      return false
    } else {
      wx.showLoading({
        mask: true,
        title: app.globalData.loadText,
      })
      that.setData({
        currentIndex: index
      })
      if(index == 0) {
        that.initApp(index)
      }
      if (index == 1) {
        that.initApp(index)
      }
    }
  },

  /**
   * 
   */
  initApp: function (i) {
    let that = this,
        body
    if(i == 1) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/company-des',
        success: res => {
          body = res.data.data.company[0].content
          WxParse.wxParse('content', 'html', body, that, 5);
          that.setData({
            aboutImg: res.data.data.company[0].image,
            aboutTitle: res.data.data.company[0].title,
            aboutContent: body
          })
          wx.hideLoading()
        }
      })
    }
    if (i == 0) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/course/index',
        success: res => {
          console.log(res)
          if(res.data.status == 1) {
            that.setData({
              courseList: res.data.data.course
            })
            wx.hideLoading()
          }
        }
      })
    }
    if (i == 2) {
      wx.request({
        url: app.globalData.hostAPI + '/v1/course/list',
        success: res => {
          console.log(res)
          that.setData({
            bmList: res.data.data.course
          })
          wx.hideLoading()
        }
      })
    }
  },

  /**
   * 
   */
  navtoDetail: function(e) {
    let that = this
    wx.navigateTo({
      url: './list/list?id=' + e.currentTarget.id + '&cid=' + e.currentTarget.dataset.cid,
    })
  }
})